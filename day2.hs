-- https://adventofcode.com/2022/day/2
module Day2 where

import System.Environment ()

data RPS = Rock | Paper | Scissors deriving (Eq)
data Outcome = Win | Lose | Draw deriving (Eq)

pointsRPS :: RPS -> Int
pointsRPS x =
  case x of
    Rock     -> 1
    Paper    -> 2
    Scissors -> 3

pointsOutcome :: Outcome -> Int
pointsOutcome x =
  case x of
    Lose -> 0
    Win  -> 6
    Draw -> 3

parseRPS1 :: Char -> RPS
parseRPS1 c
  | c == 'A' || c == 'X' = Rock
  | c == 'B' || c == 'Y' = Paper
  | c == 'C' || c == 'Z' = Scissors
  | otherwise = undefined

roundRPS :: (RPS, RPS) -> Outcome
roundRPS (x, y)
  | x == y                         = Draw
  | x == Rock     && y == Paper    = Win
  | x == Paper    && y == Scissors = Win
  | x == Scissors && y == Rock     = Win
  | otherwise                      = Lose

pointsTotal1 :: Maybe (Char, Char) -> Int
pointsTotal1 Nothing = 0
pointsTotal1 (Just (opp, own)) = rpsPoints + outcomePoints
  where
    rpsOpp = parseRPS1 opp
    rpsOwn = parseRPS1 own
    rpsPoints = pointsRPS rpsOwn
    outcomePoints = pointsOutcome outcome
    outcome = roundRPS (rpsOpp, rpsOwn)

pointsTotal2 :: Maybe (Char, Char) -> Int
pointsTotal2 Nothing = 0
pointsTotal2 (Just (opp, own)) = rpsPoints + outcomePoints
  where
    rpsOpp = parseRPS1 opp
    rpsOwn = rpsStrategy rpsOpp (parseOutcome own)
    rpsPoints = pointsRPS rpsOwn
    outcomePoints = pointsOutcome outcome
    outcome = roundRPS (rpsOpp, rpsOwn)

parseOutcome :: Char -> Outcome
parseOutcome c
  | c == 'X' = Lose
  | c == 'Y' = Draw
  | c == 'Z' = Win
  | otherwise = undefined

loseRPS :: RPS -> RPS
loseRPS rps =
  case rps of
    Rock -> Scissors
    Paper -> Rock
    Scissors -> Paper

winRPS :: RPS -> RPS
winRPS rps =
  case rps of
    Rock -> Paper
    Paper -> Scissors
    Scissors -> Rock

rpsStrategy :: RPS -> Outcome -> RPS
rpsStrategy rps outcome
  | outcome == Draw = rps
  | outcome == Lose = loseRPS rps
  | outcome == Win = winRPS rps
  | otherwise = undefined

splitTuple :: String -> Maybe (Char, Char)
splitTuple s = tupleFromSplitted $ map head $ splitWhen (== ' ') s

splitWhen :: (a -> Bool) -> [a] -> [[a]]
splitWhen pred = splitWhen' pred []

splitWhen' :: (a -> Bool) -> [a] -> [a] -> [[a]]
splitWhen' pred curr [] = [curr]
splitWhen' pred curr (x : xs)
  | not $ pred x = splitWhen' pred (x : curr) xs
  | otherwise = curr : splitWhen' pred [] xs

tupleFromSplitted :: [a] -> Maybe (a, a)
tupleFromSplitted [x, y] = Just (x, y)
tupleFromSplitted _ = Nothing

part1 :: IO Int
part1 = (sum . map (pointsTotal1 . splitTuple)) . lines <$> readFile "day2.txt"

part2 :: IO Int
part2 = (sum . map (pointsTotal2 . splitTuple)) . lines <$> readFile "day2.txt"
