-- https://adventofcode.com/2022/day/3
module Day3 where

import System.Environment ()

toHalves :: String -> (String, String)
toHalves = halves

halves :: [a] -> ([a], [a])
halves xs = splitAt (div (length xs) 2) xs

priorities :: [(Char, Int)]
priorities = zip ['a'..'z'] [1..26] ++ zip ['A'..'Z'] [27..52]

findPriority :: Maybe Char -> Maybe Int
findPriority Nothing = Nothing
findPriority (Just c)
  | null found = Nothing
  | otherwise  = Just . snd $ head found
  where
    found = filter ((==c) . fst) priorities

getCommonChars :: (String, String) -> Maybe [Char]
getCommonChars (s, t)
  | null found = Nothing
  | otherwise  = Just found
  where
    found = filter (`elem` s) t

getSameChar :: (String, String) -> Maybe Char
getSameChar t = do
   commonChars <- getCommonChars t
   pure $ head commonChars

linesToGroups :: [String] -> [(String, String, String)]
linesToGroups [] =  []
linesToGroups [x] =  []
linesToGroups [x,y] =  []
linesToGroups (x:y:z:xs) = (x, y, z) : linesToGroups xs

groupToChar :: (String, String, String) -> Maybe Char
groupToChar (x, y, z) = do
  commonXY <- getCommonChars (x, y)
  commonXZ <- getCommonChars (x, z)
  getSameChar (commonXY, commonXZ)


part1 :: IO ()
part1 = do
  ps <- fmap sum . map (findPriority . getSameChar . toHalves) . lines <$> readFile "day3.txt"
  print $ sum ps

part2 :: IO ()
part2 = do
  ps <- fmap sum . map (findPriority . groupToChar) . linesToGroups . lines <$> readFile "day3.txt"
  print $ sum ps
