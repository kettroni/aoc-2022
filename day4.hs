-- https://adventofcode.com/2022/day/3
module Day4 where

import System.Environment ()
import Text.Read (readMaybe)

readMaybeInt :: String -> Maybe Int
readMaybeInt = readMaybe

splitToTuplePairs :: String -> ((Maybe Int, Maybe Int), (Maybe Int, Maybe Int))
splitToTuplePairs s = (head tuples, head $ tail tuples)
  where
    tuples = map (readToTuples . splitWhen (== '-')) pairs
    pairs = splitWhen (== ',') s
    readToTuples = \x -> (readMaybeInt $ head x, readMaybeInt $ head $ tail x)

tuplePairOverlaps :: ((Maybe Int, Maybe Int), (Maybe Int, Maybe Int)) -> Bool
tuplePairOverlaps ((Just x1, Just x2), (Just y1, Just y2))
  | y1 <= x1 && x2 <= y2 = True
  | x1 <= y1 && y2 <= x2 = True
  | otherwise = False
tuplePairOverlaps _ = False

tuplePairOverlaps2 :: ((Maybe Int, Maybe Int), (Maybe Int, Maybe Int)) -> Bool
tuplePairOverlaps2 ((Just x1, Just x2), (Just y1, Just y2))
  | elem x1 [y1 .. y2] || elem x2 [y1 .. y2] = True
  | elem y1 [x1 .. x2] || elem y2 [x1 .. x2] = True
  | otherwise = False
tuplePairOverlaps2 _ = False

splitWhen :: (a -> Bool) -> [a] -> [[a]]
splitWhen pred = reverse . splitWhen' pred []

splitWhen' :: (a -> Bool) -> [a] -> [a] -> [[a]]
splitWhen' pred curr [] = [curr]
splitWhen' pred curr (x : xs)
  | not $ pred x = splitWhen' pred (curr ++ [x]) xs
  | otherwise = splitWhen' pred [] xs ++ [curr]

part1 :: IO ()
part1 = do
  tuples <- filter tuplePairOverlaps . fmap splitToTuplePairs . lines <$> readFile "day4.txt"
  print $ length tuples

part2 :: IO ()
part2 = do
  tuples <- filter tuplePairOverlaps2 . fmap splitToTuplePairs . lines <$> readFile "day4.txt"
  print $ length tuples
