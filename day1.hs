-- https://adventofcode.com/2022/day/1
module Day1 where

import Text.Read ( readMaybe )
import Data.List ( sort )
import System.Environment ()

-- Types
newtype Calories = Calories Int
  deriving(Show, Eq)

instance Semigroup Calories where
  (Calories x) <> (Calories y) = Calories (x+y)

newtype Inventory = Inventory [Calories]
  deriving(Show)

splitWhen :: (a -> Bool) -> [a] -> [[a]]
splitWhen pred = splitWhen' pred []

splitWhen' :: (a -> Bool) -> [a] -> [a] -> [[a]]
splitWhen' pred curr [] = [curr]
splitWhen' pred curr (x:xs)
  | not $ pred x = splitWhen' pred (x:curr) xs
  | otherwise = curr : splitWhen' pred [] xs

readMaybeInt :: String -> Maybe Int
readMaybeInt = readMaybe

maybeIntToCalories :: Maybe Int -> Calories
maybeIntToCalories (Just x) = Calories x
maybeIntToCalories Nothing = Calories 0

inventorySum :: Inventory -> Int
inventorySum (Inventory xs) = caloriesToInt $ foldr (<>) (Calories 0) xs

caloriesToInt :: Calories -> Int
caloriesToInt (Calories x) = x

part1 :: IO Int
part1 = maximum . fmap inventorySum <$> inventories
  where
    inventories = fmap (Inventory . map maybeIntToCalories) <$> splittedInts
    splittedInts = splitWhen (== Nothing) <$> maybeInts
    maybeInts = map readMaybeInt <$> fileLines
    fileLines = lines <$> fileString
    fileString = readFile "day1.txt"

part2 :: IO Int
part2 = sum . take 3 . reverse . sort . fmap inventorySum <$> inventories
  where
    inventories = fmap (Inventory . map maybeIntToCalories) <$> splittedInts
    splittedInts = splitWhen (== Nothing) <$> maybeInts
    maybeInts = map readMaybeInt <$> fileLines
    fileLines = lines <$> fileString
    fileString = readFile "day1.txt"
